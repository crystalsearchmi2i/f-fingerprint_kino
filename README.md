# f-fingerprint function #

# 1. pure python package (recommend)
# 2. fortran program (obsolete)

original code by Dr. Hou and 
modified by Kino

## pure python package ##
run purepython-fingerprint.ipynb in jupyter notebook

## python interface to the fortran program (fast but obsolete) ##

```
$ export F_FINGERPRINT=/home/kino/work/f-fingerprint/cal_fingerprint
```
or place cal_fingerprint in the same directory as that of CalFingerPrint.py.

and run
```
$ ./CalFingerPrint.py
```

### How to use ###
    
```
   fingerprint = CalFingerPrintFunction('POSCAR.mp-170_HoCd')
```
or 
```
   fingerprint= CalFingerPrintFunction('POSCAR.mp-170_HoCd',npoints=150,rmax=0.1)
```
and 
```
   print (fingerprint.ffpf )
```
The output is a vector of size 300 (=3*100 ). 100=npoints and 3=the number of combination from two atoms, which are Ho-Ho, Ho-Cd and Cd-Cd. 
```
[-1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -0.9998111, -0.34059955, 56.86647549, 126.66417453, 6.08068487, -0.99012708, -0.99999965, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -0.99994653, -0.70593255, 39.65681994, 140.31363573, 11.34806619, -0.97287448, -0.9999985, -1.0, -1.0, -1.0, -1.0, -1.0, -0.99999962, -0.99183465, 3.44230231, 59.75830951, 19.89139635, -0.81941009, -0.99996076, -1.0, -1.0, -1.0, -1.0, -0.9999014, -0.73854293, 16.42880976, 28.20782955, 0.23053808, -0.99869667, -0.99999997, -1.0, -1.0, -0.999953, -0.74995469, 32.43979503, 111.42754027, 8.50267828, -0.9798078, -0.99999892, -1.0, -0.99999866, -0.97795112, 8.15265215, 94.51500842, 24.05881295, -0.83472256, -0.99997259, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -0.99999996, -0.99735234, 3.18639771, 165.41152276, 165.29933785, 3.17793674, -0.99736126, -0.99999996, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -0.99999974, -0.99098723, 6.90631978, 173.36323409, 95.67176997, 0.34743753, -0.99952785, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -0.99961635, -0.03693781, 59.77725805, 95.42554238, 2.8459824, -0.99614356, -0.9999999, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -0.99998234, -0.86853381, 23.60946007, 114.81220176, 12.70158364, -0.95924777, -0.99999695, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -0.9998111, -0.34059955, 56.86647549, 126.66417453, 6.08068487, -0.99012708, -0.99999965, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -0.99994653, -0.70593255, 39.65681994, 140.31363573, 11.34806619, -0.97287448, -0.9999985, -1.0, -1.0, -1.0, -1.0, -1.0, -0.99999962, -0.99183465, 3.44230231, 59.75830951, 19.89139635, -0.81941009, -0.99996076, -1.0, -1.0, -1.0, -1.0, -0.9999014, -0.73854293, 16.42880976, 28.20782955, 0.23053808, -0.99869667, -0.99999997, -1.0, -1.0, -0.999953, -0.74995469, 32.43979503, 111.42754027, 8.50267828, -0.9798078, -0.99999892, -1.0, -0.99999866, -0.97795112, 8.15265215, 94.51500842, 24.05881295, -0.83472256, -0.99997259, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0]
```
```
   print (fingerprint.geo )
```
will also show values of feature_geo.dat 

## cal_fingerprint fortran program ##
The fortran program is called inside CalFingerprint.py

e.g., 
```
$ ./cal_fingerprint POSCAR_name
$ ./cal_fingerprint POSCAR_name  -rmin 0.6  -npoints 150 
```
### parameters and their default values for cal_fingerprint.f90 ###
options, their defalt values and explanations

* -rmin 0.5  : the min. value of r 
* -rmax 10.0  : the max. value of r.
* -npoints 100 : the number of interval to calculate RDF
* -sigma  0.05  : width of gaussian smearing, not delta function, but gaussian smearing is applied for RDF. 
* POSCAR_name POSCAR

* -nrepeat_min 1
* -nrepeat_max 10  

nrepeat_XXX are used for supercell construction. Change it if an error occurs. 
#!/usr/bin/env /usr/bin/python3

#
# both for python2 and python3
#
# for bash 
# export F_FINGERPRINT=/home/kino/work/f-fingerprint/cal_fingerprint


from __future__ import print_function
import subprocess
import os
import sys

class  CalFingerPrintFunction(object):

    def __init__(self,filename,**kwargs):
        self.done=False
        self.run(filename,**kwargs)
        self.ffpf = self.get_ffpf()
        self.geo  = self.get_geo()

    def run(self,filename,**kwargs):
        """ run f-fingerprint program"""
        # load enviromental variable
        prog_name = "cal_fingerprint"
        key="F_FINGERPRINT"
        if key in  os.environ: 
           prog =  os.environ[key]
        else:
           dir_ = os.path.dirname(os.path.abspath(__file__))
           prog = os.path.join(dir_,prog_name)
           found_prog = False
           if  os.path.exists(prog):
               if  os.path.isfile(prog):
                   found_prog = True
           if not found_prog: 
                   print("failed to find prog", prog)
                   print("set enviromental variable F_FINGERPRINT or place ./cal_fingerprint at the same directory as the script")
                   sys.exit(-1)

        arg=[]
        #print ("kwargs",kwargs)
        #print (kwargs is not None)
        if kwargs is not None:
            for key in kwargs:
                value = kwargs[key]
                #print ("%s == %s" %(key,value))
                if True:
                    value = str(value)
                    arg.extend(["-"+key,value] )

        cmdarg=[prog,filename]
        cmdarg.extend(arg)
        #cmdarg.extend( ["> o.fingerprint", "2> e.fingerprint"] ) 
        cmdarg.extend( [">o.fingerprint"] ) 
        cmd = " ".join( cmdarg )
        print (cmd)
        try:
          ret = subprocess.call( cmd, shell=True )
          #ret = subprocess.call( cmdarg )
          if ret!=0:
              print ("returncode=",ret)
              print ("failed to run cmd")
              print ("cmd=", cmd ) 
              print ("Did you set the environmental variable",key,"?")
              raise Exception("failed to run cmd") 
              sys.exit(150)
        except OSError as e:
          print ("cmd=", cmd ) 
          print ("execution failed, e=",e)
          print ("Did you set the environmental variable",key,"?")
          raise Exception("failed to run cmd") 
          sys.exit(250)
        self.done=True
        #print ("successfully done")


    def get_ffpf(self):
        value = []
        if self.done: 
            filename="feature_ffpf.dat"
            with open(filename,"rb") as f:
                line=f.read()
                s=line.decode('utf-8').split()
                values=list(map(float,s)) # list is necessary in python3
                #print ("values=",values)
        return values 

    def get_geo(self):
        value = []
        if self.done: 
            filename="feature_geo.dat"
            with open(filename,"rb") as f:
                line=f.read()
                s=line.decode('utf-8').split()
                values=list(map(float,s)) # list is necessary in python3
                #print ("values=",values)
        return values 

if __name__ == '__main__':

    #fingerprint = CalFingerPrintFunction('sample/POSCAR.mp-170_HoCd',npoints=150,showparam=True)
    #fingerprint= CalFingerPrintFunction('sample/POSCAR.mp-170_HoCd',npoints=150)
    fingerprint = CalFingerPrintFunction('sample/POSCAR.mp-170_HoCd')


    value = fingerprint.ffpf
    print ("length of fingerprint=",len(value) )
    print ("fingerprint=",value)

    value = fingerprint.geo
    print ("length of fingerprint=",len(value) )
    print ("fingerprint=",value)

